package controllers

import javax.inject.{Inject, Singleton}

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc.{Action, Controller, Request}

import scala.collection.JavaConverters._
import scala.collection.concurrent
import java.util.concurrent.ConcurrentHashMap

@Singleton
class ProductController @Inject() extends Controller {

  type Products = Map[String, Map[String, String]]

  val catelogue: concurrent.Map[String, Products] = new ConcurrentHashMap[String, Products]().asScala

  catelogue.putIfAbsent("processors", Map(
    "i5 6400" -> Map("speed" -> "3.3Ghz", "cache" -> "6M"),
    "i5 4400" -> Map("speed" -> "3.2Ghz", "cache" -> "6M"))
  )
  catelogue.putIfAbsent("graphic cards", Map(
    "Geforce 1050" -> Map("memory" -> "4GB", "cores" -> "768"),
    "Radeon R7" -> Map("memory" -> "4GB", "memory-type" -> "DDR3"))
  )

  def categories = Action {
    Ok(Json.toJson(catelogue.mapValues(_.size)))
  }

  def categoryDetail(category: String) = Action {
    catelogue.get(category) match {
      case Some(productsFound) => Ok(Json.toJson(Map(category -> productsFound.keys)))
      case None => NotFound(Json.toJson(Map(("error" -> s"Category $category is not found"))))
    }
  }

  def productByTitle(title: String) = Action {
    val maybeProduct = catelogue.values.flatten.toMap.filterKeys(_.equalsIgnoreCase(title))
    if (maybeProduct.isEmpty) NotFound(Json.toJson(Map("error" -> s"Product $title is not found")))
    else Ok(Json.toJson(maybeProduct))
  }

  def addProduct(category: String) = Action(parse.json) { implicit request =>
    val result: Either[String, Unit] = for {
      title <- maybeTitle.right
      productsByCategory <- mayBeProductsByCategory(title, category).right
    } yield addProductToCategory(title, category, productsByCategory)

    result match {
      case Left(e) => BadRequest(Json.toJson(Map(("error" -> e))))
      case Right(_) => Created(Json.toJson(Map("message" -> s"${maybeTitle.right.get} is added to the $category")))
    }

  }

  private def maybeTitle(implicit request: Request[JsValue]): Either[String, String] = (request.body \ "title").validate[String] match {
    case s: JsSuccess[String] => {
      if (s.get.trim.isEmpty) Left("title is empty")
      else Right(s.get.trim)
    }
    case e: JsError => Left(s"$e")
  }

  private def mayBeProductsByCategory(title: String, category: String): Either[String, Products] = catelogue.get(category) match {
    case None => Left(s"Category $category is not found)")
    case Some(productsByCategory) => {
      if (productsByCategory.get(title).isDefined) Left(s"Product $title is already in the $category category")
      else Right(productsByCategory)
    }
  }

  private def addProductToCategory(title: String, category: String, productsByCategory: Products) =
    catelogue.update(category, productsByCategory + (title -> Map.empty))


}
