import akka.stream.Materializer
import controllers.ProductController
import org.scalatestplus.play.{OneServerPerTest, PlaySpec, _}
import play.api.libs.json.{JsArray, Json}
import play.api.test.FakeRequest
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.Future

class ProductControllerSpec extends PlaySpec with OneServerPerTest {

  "GET /categories" should {
    "retrieve all categories along with number of products in the category" in {
      val controller = new ProductController()
      val result: Future[Result] = controller.categories().apply(FakeRequest())
      val categories = contentAsJson(result)
      (categories \ "processors").as[Int] mustBe 2
      (categories \ "graphic cards").as[Int] mustBe 2
    }
  }

  "GET /categories/:category" should {
    "return NotFound if the given category is not found" in {
      val controller = new ProductController()
      val result: Future[Result] = controller.categoryDetail("nonExistingCategory").apply(FakeRequest())
      status(result) mustBe NOT_FOUND
    }
    "for a given category name, retrieve a category along with the product titles " in {
      val controller = new ProductController()
      val result: Future[Result] = controller.categoryDetail("processors").apply(FakeRequest())
      status(result) mustBe OK
      val category = (contentAsJson(result) \ "processors").as[List[String]]
      category contains ("i5 6400") mustBe true
      category contains ("i5 4400") mustBe true
    }
  }

  "GET /products/:title" should {
    "return NotFound if no product can be found for a given product title" in {
      val controller = new ProductController()
      val result = controller.productByTitle("non existing title").apply(FakeRequest())
      status(result) mustBe NOT_FOUND
    }
    "for a given product title(ignore case), retrieve a product with its attributes" in {
      val controller = new ProductController()
      val result = controller.productByTitle("GEFORCE 1050").apply(FakeRequest())
      status(result) mustBe OK
      val product = (contentAsJson(result) \ "Geforce 1050")
      (product \ "memory").as[String] mustBe "4GB"
      (product \ "cores").as[String] mustBe "768"
    }
  }

  "POST /products/:category" should {
    "return BadRequest if the category can't be found" in {
      implicit lazy val materializer: Materializer = app.materializer
      val controller = new ProductController()
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "title": "abc" }"""))
      val result = call(controller.addProduct("nonExistingCategory"), request)
      status(result) mustBe BAD_REQUEST
    }
    "return BadRequest if the title is missing in the json payload" in {
      implicit lazy val materializer: Materializer = app.materializer
      val controller = new ProductController()
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ }"""))
      val result = call(controller.addProduct("processors"), request)
      status(result) mustBe BAD_REQUEST
    }
    "return BadRequest if the title is empty string" in {
      implicit lazy val materializer: Materializer = app.materializer
      val controller = new ProductController()
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "title": "  "}"""))
      val result = call(controller.addProduct("processors"), request)
      status(result) mustBe BAD_REQUEST
    }
    "return BadRequest if the product title is already exist in the category" in {
      implicit lazy val materializer: Materializer = app.materializer
      val controller = new ProductController()
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "title": "i5 6400"}"""))
      val result = call(controller.addProduct("processors"), request)
      status(result) mustBe BAD_REQUEST
    }
    "add a product to a category" in {
      implicit lazy val materializer: Materializer = app.materializer
      val controller = new ProductController()
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "title": "abc" }"""))
      val result = call(controller.addProduct("processors"), request)
      status(result) mustBe CREATED

      controller.catelogue.get("processors").get.get("abc").isDefined mustBe true
    }

  }


}
